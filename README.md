# dbfetcher

Пакет для выгрузки данных из PostgreSQL в бинарный формат. Выгрузки прежде всего предназначены для использования в shiny-приложениях, но также могут использоваться в других сервисах и исследовательских задачах.

## Установка

Пакет можно установить из гит-репозитория с помощью следующей команды в R-терминале:

```r
remotes::install_gitlab("artemklevtsov/dbfetcher")
```

## Использование

```r
dbfetcher::process(dbfetcher::read_config("/path/to/config.yml"))
```

## Файл настроек

Пример файла настроек можно получить с помощью следюущей команды в R-терминале:

```r
system.file("config.yml", package = "dbfetcher")
```

Файл настроек представляет собой YAML-файл, которые должен в обязательном порядке содержать 3 секции:

- `app`: общие настройки сервиса.
- `db`: параметры подключения к базе данных.
- `tasks`: список заданий для выгрузки.

В качестве значений параметров допускается использовать R-выражения (подробнее см. в `help("yaml.load", "yaml")`), например:

```yaml
date: !expr Sys.Date()
```

### Общие настройки

Общие настройки находятся в секции `app` и включают в себя настройки логирования и алгоритма обработки заданий (параллельно или последовательно).

- `log_level`: уровень логирования (подробнее см. в `help("get_log_levels", "lgr")`).
- `parallel`: включает параллельную обработку заданий.
- `cores`: количество ядер процессора, которые будут использованы при параллельной обработке заданий (по умолчанию половина всех имеющихя ядер).

### Подключение к базе данных

Для подключения к базе данных используется драйвер, предоставленный пакетом `RPostgres`.

Настройки подключения к базе данных находятся в секции `db` и включают в себя следующие параметры (подробнее см. `help("dbConnect,PqDriver-method", "RPostgres")`):

- `host`: адрес сервера базы данных.
- `port`: порт сервера базы данных.
- `dbname`: имя базы данных.
- `user`: имя пользователя базы данных.
- `password`: пароль пользователя базы данных.

В этом разделе также могут быть указаны дополнительные параметры, поддерживаемые драйвером.

Секция `db` также может быть указана внутри задания.

### Задания

Каждое задание должно содержать поля `id`, `sql` и `data`:

- `id`: название задания (произвольная строка).
- `sql`: путь к SQL-скрипту.
- `data`: путь к выходному файлу с данными.

Если используются относительные пути к файлу, то их следует определить **относиетльно местоположения файла `config.yml`**.

Сохранение данных производится с помощью функции `qs::qsave` с параметрами по умолчанию.

Дополнительные необязательные поля:

- `description`: описание задания.
- `process`: путь к R-скрипту постобработки данных.
- `run_if`: выполнить задание если выполняется логическое условие.
- `expire`: период устаревания данных (формат: `\d+(minute|hour|day|week|month)s?`; пример: `1days`).
- `update_every`: с какой периодичностью обновлять данные (формат: `(minute|hour|day|week|month)`; пример: `day`).
- `params`: список параметров, передаваемых в SQL-запрос ([см. подробнее](https://www.postgresql.org/docs/9.2/sql-prepare.html)).
- `load_prev`: флаг о необходимости загрузки ранее сохранённых данных (объект доступен как `.old_data`).

Вызов R-скрипта постобработки происходит только в том случе, если запрос вернул хотя бы одну строку, а также путь к файлу скрипта указан корректно. В пакете предусмотрена корректная работа только со скриптами в кодировке **UTF-8**.

При вызове R-скрипта постобработки создаётся новое окружение, то есть каждый скрипт выполняется в изолированном окружении. Полученные данные копируется в переменную `.data` созданного окружения. С данной переменной и прозводятся все манипулции (изменение столбцов или строк, в том числе их количества). В случае, если полученная в результате работы скрипта переменная `.data` отличается от переданной в скрипт, то она будет записана в исходный файл. Если указан параметр `load_prev` и существует файл с предыдушей загрузкой, то он будет доступен как объект `.old_data`.

Дополнительные параметры SQL-запроса (`params`) представляют собой **неименованный** список параметров, имеющих одинаковую длинну. Важно учитывать тип передаваемых данных (между R и драйвером БД).

При использовании параметра `expire` сраниваются текущее время и время последнего изменения файла (если он существует). В случае, если разница превышает указанный интервал, данные обновляются.

При использовании `update_every` текущее время и время последнего изменения файла округляются (trunc) до указанного юнита и затем сравниваются. В случае, если текущее время превышает время последнего изменения файла (после округления), файл обновляется.

При использовании `run_if` перед выражением нужно использовать `!expr`, чтобы при чтении конфига оно выполнилось.

Пример списка заданий:

```yaml
tasks:
  - id: Test task 1
    description: Description of task 1
    sql: sql/test-1.sql
    data: data/test-1.qs
    process: R/post-test-1.R
    expire: 1days
  - id: Test task 2
    description: Description of task 2
    sql: sql/test-2.sql
    data: data/test-2.gz
    params:
      - !expr Sys.Date() - 1
      - !expr Sys.Date() - 8
  - id: task_3
    description: Test task which run onlu 
    sql: sql/query_3.sql
    data: data/data_3.qs
    process: R/sciprt.R
    run_if: !expr format(Sys.Date(), '%d') == "01"
    params:
      - !expr format(Sys.Date() - period("30days"))
```

Порядок действий по добавлении нового задания:

1. Создать SQL-скрипт с запросом.
1. Внести запись в файл настроек, указав обязательные поля `id`, `sql`, `data`.
1. (Не обязательно) добавить скрипт постобработки и добавить его в поле `process` соответствующего задания.


## Развёртывание

### Установка зависимостей

Устанавливаем системные зависимости (на примере Ubuntu LTS 18.04):

```bash
sudo apt-get update
sudo apt-get install -y gnupg git-core software-properties-common
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
sudo apt-get update
sudo apt-get install -y --no-install-recommends r-base r-base-dev
```

Устанавлиаем пакет `dbfetcher`:

```bash
Rscript -e "isntall.packages('remotes')"
Rscript -e "remotes::install_gtilab('artemklevtsov/dbfecther')"
DBFETCHER_BIN=$(Rscript -e 'cat(system.file("bin", "dbfetcher.R", package = "dbfetcher"))')
sudo chmod +x "${DBFETCHER_BIN}"
sudo ln -s "${DBFETCHER_BIN}" /usr/local/bin/dbfetcher
```

### Запуск в ручном режиме

Запускаем сервис:

```bash
CONF_PATH=/etc/dbfetcher/config.yml
dbfetcher --config ${CONF_PATH} run
```

### Запуск по расписанию с помощью systemd

Формируем директорию с настройками:

```bash
CONFDIR=/etc/dbfetcher
sudo mkdir ${CONFDIR}
sudo chmod 700 ${CONFDIR}
sudo touch ${CONFDIR}/config.yml
# edit conf file
```

Редактируем файл настроек.

Также можно использовать переменные окружения для передачи секретных данных в скрипт.

```bash
sudo touch ${CONFDIR}/.env
echo "PGHOST=host" | sudo tee -a ${CONFDIR}/.env
echo "PGPORT=port" | sudo tee -a ${CONFDIR}/.env
echo "PGDATABASE=db" | sudo tee -a ${CONFDIR}/.env
echo "PGUSER=user" | sudo tee -a ${CONFDIR}/.env
echo "PGPASSWORD=pwd" | sudo tee -a ${CONFDIR}/.env
```

В конфиге можно указать, например, такой вариант параметров подключения к базе данных:

```yaml
db:
  drv: RPostgres::Postgres
  host: !expr Sys.getenv("PGHOST")
  port: !expr Sys.getenv("PGPORT")
  dbname: !expr Sys.getenv("PGDATABASE")
  user: !expr Sys.getenv("PGUSER")
  password: !expr Sys.getenv("PGPASSWORD")
  timezone: UTC
  bigint: numeric
```

Пример файла сервиса (`/etc/systemd/system/dbfetcher.service`):

```ini
[Unit]
Description=Database fetcher service

[Service]
Type=simple
Nice=10
TimeoutStartSec=0
Restart=on-failure
RestartSec=60min
EnvironmentFile=/etc/dbfetcher/.env
ExecStart=/usr/local/bin/dbfetcher --config /etc/dbfetcher/config.yml run
```

Пример файла таймера (`/etc/systemd/system/dbfetcher.timer`):

```ini
[Unit]
Description=Database fetcher timer

[Timer]
OnCalendar=*-*-* 4:00:00
Persistent=true

[Install]
WantedBy=timers.target
```

Активируем и запускаем таймер:

```bash
sudo systemctl enable dbfetcher.timer
sudo systemctl start dbfetcher.timer
```

Просмотр таймера:

```bash
systemctl list-timers dbfetcher*
```

Просмотр журнала работы сервиса:

```bash
journalctl -o cat -u dbfetcher.service
```
